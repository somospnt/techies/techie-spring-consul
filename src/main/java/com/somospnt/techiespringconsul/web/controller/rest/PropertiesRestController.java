package com.somospnt.techiespringconsul.web.controller.rest;

import com.somospnt.techiespringconsul.config.PropertiesConfig;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class PropertiesRestController {

    private final PropertiesConfig propertiesConfig;

    @GetMapping("/properties")
    public PropertiesConfig obtener() {
        return propertiesConfig;
    }

}
