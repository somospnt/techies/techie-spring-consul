package com.somospnt.techiespringconsul;

import com.somospnt.techiespringconsul.config.PropertiesConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties(value = PropertiesConfig.class)
public class TechieSpringConsulApplication {

    public static void main(String[] args) {
        SpringApplication.run(TechieSpringConsulApplication.class, args);
    }

}
