package com.somospnt.techiespringconsul.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "com.somospnt.techiespringconsul")
public class PropertiesConfig {

    private String username;
    private String password;

}
