# Techie Spring Consul

Proyecto DEMO de una aplicación obteniendo properties desde el servcio de  Hashicorp Consul (dockerizado)

## Requisitos
1) Levantá un container de **consul** con el 
siguiente comando: `docker run -d --name=dev-consul -p 8500:8500 consul`

> Nota: También podés usar [Vagrant](https://www.vagrantup.com/downloads.html) y levantar un entorno de prueba en windows utilizando el `VagrantFile` de la carpeta */doc/vagrant*.

2) Configurá el **application.properties** del proyecto para que consuma el container anterior.

3) Corré el proyecto!
